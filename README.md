############# libreria ###########

import math
import cv2
import numpy as np
from numpy import cos, sin
from math import sqrt
import time
import random

############## CONSTANTES ########


############### PROGRAMA #################

captura = cv2.VideoCapture(0) #Iniciamos la camara
 
while(1):
     
    #Capturamos una imagen y la convertimos de RGB -> HSV
    _, imagen = captura.read()
    hsv = cv2.cvtColor(imagen, cv2.COLOR_BGR2HSV)
 
    #Establecemos el rango de colores que vamos a detectar
    #verde_bajos = np.array([49,50,50], dtype=np.uint8)
    #verde_altos = np.array([80, 255, 255], dtype=np.uint8)
    verde_bajos = np.array([0,0,0], dtype=np.uint8)
    verde_altos = np.array([500, 50, 50], dtype=np.uint8)
 
    #Crear una mascara con solo los pixeles dentro del rango de verdes
    mask = cv2.inRange(hsv, verde_bajos, verde_altos)
 
    #Encontrar el area de los objetos que detecta la camara
    moments = cv2.moments(mask)
    area = moments['m00']

 
    #Descomentar para ver el area por pantalla
    #print area
    if(area > 20000000):
        time.sleep(0.5)
        gauss = cv2.GaussianBlur(mask, (5,5), 0)
        canny = cv2.Canny(gauss, 1, 2)
        cv2.imshow("cortono", canny)
        (contornos,_) = cv2.findContours(canny, 1, 2) 
        cv2.drawContours(canny,contornos,0,(255,0,255), 2)
        #time.sleep(2) 
        #Buscamos el centro x, y del objeto
        x = int(moments['m10']/moments['m00'])
        y = int(moments['m01']/moments['m00'])
         
        #Mostramos sus coordenadas por pantalla
        print "x = ", x, " y = ", y
 
        #Dibujamos una marca en el centro del objeto
        cv2.rectangle(canny, (x, y), (x+2, y+2),(0,0,255), 2)
    #cv2.imshow("Contorno", canny)
     
    #Mostramos la imagen original con la marca del centro y
    #la mascara
    #cv2.imshow('mask', mask)
    cv2.imshow('Camara', imagen)
    tecla = cv2.waitKey(5) & 0xFF
    if tecla == 27:
        break


 
cv2.destroyAllWindows()